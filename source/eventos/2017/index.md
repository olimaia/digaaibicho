---
title: Castração de gatos em julho de 2017
photos:
	- /fotos/2017/IMG_0649-1.jpg
	- /fotos/2017/IMG_0651.jpg
	- /fotos/2017/IMG_0652.jpg
	- /fotos/2017/IMG_0653.jpg
	- /fotos/2017/IMG_0654.jpg
	- /fotos/2017/IMG_0655.jpg
	- /fotos/2017/IMG_0656.jpg
	- /fotos/2017/IMG_0657.jpg
	- /fotos/2017/IMG_0659.jpg
	- /fotos/2017/IMG_0661.jpg
	- /fotos/2017/IMG_0663.jpg
	- /fotos/2017/IMG_0665.jpg
	- /fotos/2017/IMG_0667.jpg
	- /fotos/2017/IMG_0672-1.jpg
---

No último mutirão de castração de felinos de 2017, foram realizadas 26 castrações: ao todo foram 14 machos e 12 fêmeas. O dia foi longo, mas produtivo! Foram 26 gatinhos e gatinhas que ganharam mais qualidade de vida. Agradecemos de coração a todos que participaram!

Clique para ver as fotos em tamanho maior:
