---
title: Mutirão de castração de 2018
photos:
	- /fotos/2018/img_5335.png
	- /fotos/2018/img_5341.png
	- /fotos/2018/img_5351.png
	- /fotos/2018/img_5360.png
	- /fotos/2018/img_5364.png
	- /fotos/2018/img_5366.png
	- /fotos/2018/img_5367.png
	- /fotos/2018/img_5375.png
	- /fotos/2018/img_5384.png
	- /fotos/2018/img_5397.png
	- /fotos/2018/img_5398.png
	- /fotos/2018/img_5405.png
	- /fotos/2018/img_5420.png
	- /fotos/2018/img_5432.png
	- /fotos/2018/img_5439.png
	- /fotos/2018/img_5446.png
	- /fotos/2018/img_5450.png
	- /fotos/2018/img_5455.png
	- /fotos/2018/img_5457.png
	- /fotos/2018/img_5485.png
	- /fotos/2018/img_5486.png
	- /fotos/2018/img_5517.png
	- /fotos/2018/img_5529.png
	- /fotos/2018/img_5534.png
	- /fotos/2018/img_5537.png
---

Nesta primeira campanha de castração do ano, com a parceria da médica veterinária Dra. Anne Raphaelle, a _Diga aí, Bicho_ teve a oportunidade de realizar a castração de 23 gatos e 13 cães, machos e fêmeas. O mutirão aconteceu no sábado, dia 9 de junho.

Contamos também com a ajuda do grupo escoteiro da cidade, que cuidou da recepção dos animais. A prefeitura municipal de Lençóis providenciou a energia elétrica para o Pet Móvel.
