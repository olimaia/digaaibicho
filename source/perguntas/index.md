---
title: "Mais informações sobre a castração"
---

Bicho castrado engorda? Fica preguiçoso? Meu cão vai perder a virilidade? Minha gatinha vai continuar entrando no cio? São muitas as questões que podem surgir quando conversamos sobre castração, e é importante estar bem informado para que possamos cuidar de nossos bichinhos da melhor forma possível.

Conheça alguns dos **fatos** e mitos mais comuns sobre a castração de cães e gatos:

**Castração é um ato brutal?**  
Não. As cirurgias acontecem em ambientes apropriados, sob anestesia geral e com a utilização de medicações analgésicas, anti-inflamatórias e anestésicas. Trata-se de um procedimento cirúrgico comum, realizado por profissionais capacitados. 

![Dois cirurgiões veterinários durante o processo de castração de uma gatinha em um dos mutirões.](/fotos/IMG_0672.jpg)

**O animal vai engordar depois de castrado?**  
A castração em si não é responsável pela obesidade. A obesidade depende de vários fatores, como dieta inadequada, alimentação em excesso, pouca ou nenhuma atividade física, alterações na taxa hormonal do organismo e oferta de guloseimas. O aumento do peso após a castração também está relacionado à propensão racial (mais comum em Beagle, Cocker Spaniel, Dachshunds e Labrador) e à castração em idade avançada. Também está ligada ao sedentarismo do proprietário. O problema da obesidade pode ser minimizado com o controle qualitativo e quantitativo da dieta e com atividade física. Castrar o animal antes dos cinco meses de idade também ajuda a evitar que ele engorde após o procedimento.

**Castração custa caro?**  
Não. Além de o valor da cirurgia ser muito menor do que os custos envolvidos em se cuidar de fêmeas em gestação ou ninhadas acidentais, ou mesmo de animais que sofreram lesão corporal por estarem soltos na rua, existem clínicas e serviços especializados em controle populacional que adotam preços acessíveis.

**Macho castrado perde a masculinidade?**  
Machos castrados não deixam de ser machos e muito menos mudam de sexo: eles perdem apenas o instinto de procriação.

**Fêmeas devem ter pelo menos uma cria antes de serem castradas?**  
Quanto mais cedo a fêmea for esterilizada, menor será a chance de desenvolver tumor mamário. Por isso, o ideal é que a castração ocorra antes do primeiro cio, por volta dos seis meses de idade.

**Animal castrado perambula menos?**  
Sim. Cães e gatos machos saem atrás de fêmeas no rio para acasalar. Com a esterilização, os animais têm menos ânsia de perambular e, portanto, menos chances de brigarem e de sofrerem acidentes, serem maltratados e roubados, ou se perderem.

**Animal castrado não faz xixi por todos os cantos?**  
A demarcação territorial com urina é reduzida ou inexistente em animais que são castrados antes de adquirirem esse comportamento.

**Animal castrado briga menos?**  
Sim. A agressividade de machos contra outros machos é parcialmente relacionada à disputa por fêmeas no cio: trata-se, afinal, de um comportamento relacionado à atividade sexual.

**A castração reduz riscos de problemas de saúde?**  
O risco de os animais desenvolverem certos tumores em idade avançada é reduzido com a castração. A castração também evita a ocorrência de doenças sexualmente transmissíveis e reduz a disseminação de zoonoses. Em gatos, a castração após os seis meses de idade minimiza a ocorrência de estreitamento de uretra

**A castração pode causar problemas de saúde?**  
Em fêmeas caninas a vulva pode permanecer infantil, o que pode levar a problemas de pele na região, principalmente em animais obesos. Nos cães machos, o pênis e prepúcio podem não se desenvolver. Mais raramente, algumas fêmeas caninas podem apresentar dificuldade para reter urina no período de um a cinco anos após a castração, com mais frequência em cadelas de porte grande ou gigante, acima de 20 kg e com mais de 9 anos de idade. Esse problema, no entanto, pode ser tratado com reposição hormonal.

**Fêmeas castradas entram no cio?**  
A cirurgia impede que o animal entre no cio e procrie.

FONTE: _Contracepção cirúrgica em cães e gatos_, de Alfredo Feio da Maia LIMA, Stelio Pacca Loureiro LUNA e Werner John PAYNE. Editora MedVet, 2015.

**Outras leituras:**  
[Castração em cães e gatos: vantagens x desvantagens](http://www.revistaveterinaria.com.br/2012/01/19/castracao-em-caes-e-gatos-vantagens-x-desvantagens/) (Revista Veterinária)
[15 motivos para castrar uma cadela](http://dicasboaspracachorro.com.br/15-motivos-para-castrar-uma-cadela) (Dicas boas pra cachorro)
[Castração de cães machos: vantagens e desvantagens](http://www.royalcanindobrasil.com.br/enciclopet/cao-adulto/castracao-de-caes-machos/) (Royal Canin)
[Confira 5 benefícios da castração de cães e gatos](https://veja.abril.com.br/ciencia/confira-5-beneficios-da-castracao-de-caes-e-gatos/) (Revista Veja)
[Castração de gatos machos: as vantagens de se castrar um gato macho](http://www.cachorrogato.com.br/gato/castracao-gatos-machos/) (CachorroGato)
[Esterilização/castração: vantagens e desvantagens para os gatos](http://www.arcadenoe.pt/artigo/esteriliza_o_castra_o_vantagens_e_desvantagens_para_os_gatos/491) (Arca de Noé)

Caso ainda tenha dúvidas, [entre em contato conosco](mailto:digaaibicho@gmail.com)!
