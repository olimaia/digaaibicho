/**
 * Sets up Justified Gallery.
 */
if (!!$.prototype.justifiedGallery) {
  var options = {
    rowHeight: 200,
    margins: 10,
    lastRow: "nojustify"
  };
  $(".article-gallery").justifiedGallery(options);
}

$('a.gallery').featherlightGallery({
  previousIcon: '«',
  nextIcon: '»',
  galleryFadeIn: 300,
  openSpeed: 300
});

